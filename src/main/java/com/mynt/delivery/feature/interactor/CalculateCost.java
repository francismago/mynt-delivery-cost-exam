package com.mynt.delivery.feature.interactor;

import com.mynt.delivery.feature.req.DeliveryRequest;
import com.mynt.delivery.feature.resp.DeliveryResponse;

public interface CalculateCost {
    DeliveryResponse execute(DeliveryRequest deliveryRequest);
}
