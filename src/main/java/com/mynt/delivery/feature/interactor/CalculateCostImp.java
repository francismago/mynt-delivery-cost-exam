package com.mynt.delivery.feature.interactor;

import com.mynt.delivery.client.mynt.MyntVoucherClient;
import com.mynt.delivery.client.mynt.VoucherResponse;
import com.mynt.delivery.feature.exception.ParcelRejectedException;
import com.mynt.delivery.feature.exception.VoucherExpiredException;
import com.mynt.delivery.feature.req.DeliveryRequest;
import com.mynt.delivery.feature.resp.DeliveryResponse;
import com.mynt.delivery.feature.util.ParcelCostUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CalculateCostImp implements CalculateCost {

    @Autowired
    private ParcelCostUtil parcelCostUtil;

    @Autowired
    private MyntVoucherClient myntVoucherClient;

    @Value("${mynt.key}")
    private String key;

    @Override
    public DeliveryResponse execute(DeliveryRequest deliveryRequest) {
        double weight = deliveryRequest.getWeight();
        double volume = calculateVolume(deliveryRequest);
        double cost = 0;
        if (isReject(weight)) {
            throw new ParcelRejectedException();
        } else if (isHeavyParcel(weight)) {
            cost = parcelCostUtil.getHeavy() * weight;
        } else if (isSmallParcel(volume)) {
            cost = parcelCostUtil.getSmall() * volume;
        } else if (isMediumParcel(volume)) {
            cost = parcelCostUtil.getMedium() * volume;
        } else if (isLargeParcel(volume)) {
            cost = parcelCostUtil.getLarge() * volume;
        }
        if (deliveryRequest.getVoucherCode() != null) {
            cost -= getDiscount(deliveryRequest.getVoucherCode());
            if (cost < 0) {
                cost = 0;
            }
        }


        return new DeliveryResponse(cost);
    }

    private double getDiscount(String voucherCode) {
        VoucherResponse resp = myntVoucherClient.getVoucher(key, voucherCode);

        if (LocalDate.now().plusDays(1).isAfter(LocalDate.parse(resp.getExpiry()))) {
            throw new VoucherExpiredException();
        }
        return resp.getDiscount();
    }

    private double calculateVolume(DeliveryRequest dr) {
        return dr.getHeight() * dr.getWidth() * dr.getLength();
    }

    private boolean isReject(double weight) {
        return weight > 50;
    }

    private boolean isHeavyParcel(double weight) {
        return weight > 10;
    }

    private boolean isSmallParcel(double volume) {
        return volume < 1500;
    }

    private boolean isMediumParcel(double volume) {
        return volume < 2500;
    }

    private boolean isLargeParcel(double volume) {
        return volume >= 2500;
    }
}
