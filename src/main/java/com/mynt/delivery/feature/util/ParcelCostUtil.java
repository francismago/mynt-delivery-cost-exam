package com.mynt.delivery.feature.util;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ParcelCostUtil {

    @Value("${parcel-cost.heavy}")
    private double heavy;

    @Value("${parcel-cost.small}")
    private double small;

    @Value("${parcel-cost.heavy}")
    private double medium;

    @Value("${parcel-cost.heavy}")
    private double large;

}
