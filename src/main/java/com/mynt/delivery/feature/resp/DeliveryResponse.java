package com.mynt.delivery.feature.resp;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class DeliveryResponse {
    @NonNull
    private double cost;
}
