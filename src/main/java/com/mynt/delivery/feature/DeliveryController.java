package com.mynt.delivery.feature;


import com.mynt.delivery.feature.exception.ErrorMsg;
import com.mynt.delivery.feature.interactor.CalculateCost;
import com.mynt.delivery.feature.req.DeliveryRequest;
import com.mynt.delivery.feature.resp.DeliveryResponse;
import com.mynt.delivery.feature.validator.DeliveryCostParamValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Delivery API", tags = "Delivery API")
@RestController
@RequestMapping("/deliveries")
public class DeliveryController {

    @Autowired
    private CalculateCost calculateCost;

    @InitBinder("deliveryRequest")
    private void initDeliveryRequestBinder(WebDataBinder binder) {
        binder.addValidators(new DeliveryCostParamValidator());
    }

    @ApiOperation(value = "Calculate delivery cost")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = DeliveryResponse.class, message = "Success"),
            @ApiResponse(code = 400, response = ErrorMsg.class, message = "Bad Request"),
            @ApiResponse(code = 404, response = ErrorMsg.class, message = "Not Found")})
    @GetMapping("/cost")
    public DeliveryResponse calculateCost(@Validated DeliveryRequest deliveryRequest) {
        return calculateCost.execute(deliveryRequest);
    }
}
