package com.mynt.delivery.feature.validator;

import com.mynt.delivery.feature.req.DeliveryRequest;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class DeliveryCostParamValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return DeliveryRequest.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DeliveryRequest dr = (DeliveryRequest) target;
        System.out.println("dr="+dr);
        if (dr.getWeight() == 0) {
            errors.rejectValue("weight", "400", "Weight is required");
        }
        if (dr.getHeight() == 0) {
            System.out.println("hit H");
            errors.rejectValue("height", "400", "Height is required");
        }
        if (dr.getWidth() == 0.0) {
            errors.rejectValue("width", "400", "Width is required");
        }
        if (dr.getLength() ==0.0) {
            System.out.println("hit L");
            errors.rejectValue("length", "400", "Length is required");
        }
    }

}
