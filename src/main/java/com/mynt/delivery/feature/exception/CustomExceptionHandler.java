package com.mynt.delivery.feature.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ParcelRejectedException.class)
    protected ResponseEntity<ErrorMsg> handleParcelRejected(ParcelRejectedException ex) {
        return new ResponseEntity<ErrorMsg>(
                new ErrorMsg(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(VoucherExpiredException.class)
    protected ResponseEntity<ErrorMsg> handleVoucherExpired(VoucherExpiredException ex) {
        return new ResponseEntity<ErrorMsg>(
                new ErrorMsg(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(VoucherUnavailableException.class)
    protected ResponseEntity<ErrorMsg> handleVoucherUnavailableExceptiont(VoucherUnavailableException ex) {
        return new ResponseEntity<ErrorMsg>(
                new ErrorMsg(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(VoucherInvalidException.class)
    protected ResponseEntity<ErrorMsg> handleVoucherInvalidException(VoucherInvalidException ex) {
        return new ResponseEntity<ErrorMsg>(
                new ErrorMsg(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}