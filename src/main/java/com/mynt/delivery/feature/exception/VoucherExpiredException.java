package com.mynt.delivery.feature.exception;

public class VoucherExpiredException extends RuntimeException {

    public VoucherExpiredException() {
        super("Voucher expired");
    }
}
