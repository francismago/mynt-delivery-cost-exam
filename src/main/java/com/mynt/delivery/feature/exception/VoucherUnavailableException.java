package com.mynt.delivery.feature.exception;

public class VoucherUnavailableException extends RuntimeException {

    public VoucherUnavailableException() {
        super("Voucher unavailable");
    }
}
