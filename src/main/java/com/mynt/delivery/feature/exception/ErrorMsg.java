package com.mynt.delivery.feature.exception;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class ErrorMsg {
    @NonNull
    private Integer code;
    @NonNull
    private String message;
}
