package com.mynt.delivery.feature.exception;

public class VoucherInvalidException extends RuntimeException {

    public VoucherInvalidException() {
        super("Voucher invalid");
    }
}
