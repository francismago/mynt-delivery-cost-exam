package com.mynt.delivery.feature.exception;


public class ParcelRejectedException extends RuntimeException{

    public ParcelRejectedException() {
        super("Rejected Parcel");
    }
}
