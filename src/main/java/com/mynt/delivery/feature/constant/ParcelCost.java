package com.mynt.delivery.feature.constant;


public enum ParcelCost {
    HEAVY,
    SMALL,
    MEDIUM,
    LARGE;
}
