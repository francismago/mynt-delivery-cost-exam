package com.mynt.delivery.feature.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryRequest {

    @ApiModelProperty(required = true, value = "Weight (kg)")
    @NonNull
    private double weight;

    @ApiModelProperty(required = true,value = "Height (cm)")
    @NonNull
    private double height;

    @ApiModelProperty(required = true, value = "Width (cm)")
    @NonNull
    private double width;

    @ApiModelProperty(required = true, value = "Length (cm)")
    @NonNull
    private double length;

    @ApiModelProperty(value = "Voucher Code")
    private String voucherCode;
}
