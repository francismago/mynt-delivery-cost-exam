package com.mynt.delivery.client.mynt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoucherResponse {
    private String code;
    private Double discount;
    private String expiry;
}


