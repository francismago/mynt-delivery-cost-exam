package com.mynt.delivery.client.mynt;

import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "myntVoucherClient", url = "${mynt.base-url}")
@Headers("Content-Type: application/json")
public interface MyntVoucherClient {

    @RequestMapping(method = RequestMethod.GET, value = "/voucher/{voucherCode}")
    public VoucherResponse getVoucher(@RequestParam("key") String key,
                                      @PathVariable("voucherCode") String voucherCode);

}
