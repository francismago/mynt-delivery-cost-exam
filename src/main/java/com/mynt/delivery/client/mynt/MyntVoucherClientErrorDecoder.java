package com.mynt.delivery.client.mynt;

import com.mynt.delivery.feature.exception.VoucherInvalidException;
import com.mynt.delivery.feature.exception.VoucherUnavailableException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class MyntVoucherClientErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        if(response.status() == 400){
            throw new VoucherInvalidException();
        }else {
            throw new VoucherUnavailableException();
        }
    }
}
