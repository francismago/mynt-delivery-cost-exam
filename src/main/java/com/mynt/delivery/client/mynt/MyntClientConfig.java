package com.mynt.delivery.client.mynt;

import feign.codec.ErrorDecoder;
import feign.okhttp.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyntClientConfig {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new MyntVoucherClientErrorDecoder();
    }

    @Bean
    public OkHttpClient client() {
        return new OkHttpClient();
    }

}
