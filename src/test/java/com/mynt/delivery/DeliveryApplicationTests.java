package com.mynt.delivery;

import com.mynt.delivery.client.mynt.MyntVoucherClient;
import com.mynt.delivery.client.mynt.VoucherResponse;
import com.mynt.delivery.feature.interactor.CalculateCostImp;
import com.mynt.delivery.feature.req.DeliveryRequest;
import com.mynt.delivery.feature.resp.DeliveryResponse;
import com.mynt.delivery.feature.util.ParcelCostUtil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringJUnit4ClassRunner.class)
class DeliveryApplicationTests<parcelCostUtil> {


    @Mock
    MyntVoucherClient myntVoucherClient;

    @Mock
    ParcelCostUtil parcelCostUtil;

    @InjectMocks
    CalculateCostImp calculateCost;


    @Test
    public void compute_delivery_cost_should_return_success() {
        BDDMockito.given(parcelCostUtil.getSmall()).willReturn( 0.03);
        DeliveryRequest request = new DeliveryRequest(10, 10, 10, 10, null);
        DeliveryResponse response = calculateCost.execute(request);
        Assertions.assertThat(response.getCost()).isEqualTo(30);
    }

    @Test
    public void compute_delivery_cost_should_return_success_with_discount() {
        BDDMockito.given(parcelCostUtil.getSmall())
                .willReturn( 0.03);
        BDDMockito.given(myntVoucherClient.getVoucher(BDDMockito.anyObject(),BDDMockito.anyString()))
                .willReturn(new VoucherResponse("MYNT",10.0,"2021-05-05"));
        DeliveryRequest request = new DeliveryRequest(10, 10, 10, 10, "MYNT");
        DeliveryResponse response = calculateCost.execute(request);
        Assertions.assertThat(response.getCost()).isEqualTo(20);
    }

}
