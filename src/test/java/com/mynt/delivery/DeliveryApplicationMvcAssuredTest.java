package com.mynt.delivery;

import com.mynt.delivery.feature.exception.ErrorMsg;
import com.mynt.delivery.feature.resp.DeliveryResponse;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DeliveryApplicationMvcAssuredTest {

    @Autowired
    public WebApplicationContext webApplicationContext;

    @Test
    public void compute_delivery_cost_should_return_success() {
        DeliveryResponse response = RestAssuredMockMvc.
                given()
                .webAppContextSetup(webApplicationContext)
                .queryParam("height", 10)
                .queryParam("length", 10)
                .queryParam("weight", 10)
                .queryParam("width", 10)
                .get("/deliveries/cost")
                .then()
                .status(HttpStatus.OK)
                .extract()
                .as(DeliveryResponse.class);

        Assertions.assertThat(response.getCost()).isEqualTo(30);
    }

    @Test
    public void compute_delivery_cost_should_return_expired() {
        ErrorMsg response = RestAssuredMockMvc.
                given()
                .webAppContextSetup(webApplicationContext)
                .queryParam("height", 10)
                .queryParam("length", 10)
                .queryParam("weight", 10)
                .queryParam("width", 10)
                .queryParam("voucherCode", "MYNT")
                .get("/deliveries/cost")
                .then()
                .status(HttpStatus.BAD_REQUEST)
                .extract()
                .as(ErrorMsg.class);

        Assertions.assertThat(response.getCode()).isEqualTo(400);
        Assertions.assertThat(response.getMessage()).isEqualTo("Voucher expired");
    }


    @Test
    public void compute_delivery_cost_should_return_invalid() {
        ErrorMsg response = RestAssuredMockMvc.
                given()
                .webAppContextSetup(webApplicationContext)
                .queryParam("height", 10)
                .queryParam("length", 10)
                .queryParam("weight", 10)
                .queryParam("width", 10)
                .queryParam("voucherCode", "skdlks")
                .get("/deliveries/cost")
                .then()
                .status(HttpStatus.BAD_REQUEST)
                .extract()
                .as(ErrorMsg.class);

        Assertions.assertThat(response.getCode()).isEqualTo(400);
        Assertions.assertThat(response.getMessage()).isEqualTo("Voucher invalid");
    }

}
